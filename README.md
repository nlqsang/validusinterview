# VALIDUSCOIN App

### 1. Setup base project

#### - Step 1: Cd to base_app

#### - Step 2: Run ```flutter pub get```

#### - Step 3: Run ```flutter packages pub run build_runner build --delete-conflicting-outputs```

### 2. Setup main project

#### - Step 1: Cd to main_app

#### - Step 2: Run ```flutter pub get```

#### - Step 3: Run ```flutter packages pub run build_runner build --delete-conflicting-outputs```

### 2. Development script
#### - Create dart object script:

```
import 'package:json_annotation/json_annotation.dart';

part '${NAME}.g.dart';

@JsonSerializable()
class ${ObjectName}{

  factory ${ObjectName}.fromJson(Map<String, dynamic> json) => _${ObjectName}FromJson(json);

  Map<String, dynamic> toJson() => _${ObjectName}ToJson(this);
}
```
