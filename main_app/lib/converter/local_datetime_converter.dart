import 'package:base_app/logger/app_logger.dart';
import 'package:json_annotation/json_annotation.dart';

class LocalDateTimeConverter implements JsonConverter<DateTime?, String?> {
  const LocalDateTimeConverter();

  @override
  DateTime fromJson(String? json) {
    if (json != null) {
      try {
        return DateTime.parse(json).toLocal();
      } catch (ex) {
        AppLogger.e(ex);
      }
    }

    return DateTime.now();
  }

  @override
  String? toJson(DateTime? json) {
    return json?.toUtc().toIso8601String();
  }
}
