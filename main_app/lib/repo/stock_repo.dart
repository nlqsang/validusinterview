import 'package:base_app/core/base_repo.dart';
import 'package:injectable/injectable.dart';
import 'package:main_app/data_source/remote/api_client.dart';
import 'package:main_app/schema/response/api_response.dart';
import 'package:main_app/schema/stock/stock.dart';

abstract class IStockRepo extends BaseRepo {
  Future<APIResponse<List<Stock>>> getStocks();
}

@Singleton(as: IStockRepo)
class StockRepoImpl implements IStockRepo {
  final ApiClient _apiClient;

  StockRepoImpl(this._apiClient);

  @override
  Future<APIResponse<List<Stock>>> getStocks() async {
    try {
      return await _apiClient.stockService.getStocks();
    } catch (e) {
      return APIResponse.fromException(e);
    }
  }
}
