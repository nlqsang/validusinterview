import 'package:flutter/material.dart';

class ResColors {
  static var primaryColor = Color(0xFF0012B6);
  static var primaryTextColor = Color(0xFF1E272E);
  static var tabActiveColor = Color(0xFFCE863A);
  static var tabInActiveColor = Color(0xFF000000);
  static var inputGrayColor = Color(0xFF707070);
  static var successColor = Color(0xFF28A745);
  static var errorColor = Color(0xFFF44336);
  static var backgroundColor = Color(0xFFF6F4F5);
  static var bgTextInputColor = Color(0xFFF6F5F4);
  static var colorStartAdvise = Color(0xFF08B5AA);
  static var colorTextService = Color(0xFF2C3137);
  static var colorTextAllService = Color(0xFF08B5AA);
  static var colorButtonNetHub = Color(0xFFE5141B);
  static var colorContentService = Color(0xFF737377);
  static var bgTeal = Color(0xFF23C9BF);
  static var bgGreen = Color(0xFF2CA94C);
  static var bgYellow = Color(0xFFD9BD2B);
  static var colorDot = Color(0xFFA0A1A2);
  static var accent = Color(0xFFFF353C);
  static var secondary = Color(0xFFF6F4F5);
  static var contentYellow = Color(0xFFFFDE32);
  static var borderInverseOpaque = Color(0xFF454A51);
  static var contentPrimary = Color(0xFF2C3137);
  static var prettyDark = Color(0xFF070D14);
  static var tertiary = Color(0xFFEBEAEA);
  static var contentDisabled = Color(0xFFA0A1A2);
  static var red5 = Color(0xFFFFF5F5);
  static var teal = Color(0xFF23C9BF);
  static var bgRed = Color(0xFFFFF5F5);
  static var positive = Color(0xFF2CA94C);
  static var lightOrange = Color(0xFFFFF7F2);
  static var blueColor = Color(0xFF08B5AA);
  static var darkerAccent = Color(0xFFD92D33);
  static var opaque = HexColor('#EBEAEA');
  static var bgAvatar = HexColor('#EBEAEA');
  static var bgGreenBtn = HexColor('#F5FCF7');
  static var colorGreenBtn = HexColor('#64D481');
  static var colorBar = HexColor('#454A51');
  static var colorD3D2D2 = HexColor('#D3D2D2');
  static var color34C759 = HexColor('#34C759');
  static var lightBlue = HexColor('#007AFF');
  static var lightPinkRed = HexColor('#FFE9EA');
  static var bgNetbub = HexColor('#131A28');
  static var colorDeepGray = HexColor('#272B2F');

  static var clickMe = HexColor('#10B9D0');
  static var clickMeDarker = HexColor('#0EA4B9');
  static var clickMeLighter = HexColor('#52D9EB');
  static var brightBlue = HexColor('#39D0E9');

  static var endangered = HexColor('#FF2F48');
  static var endangeredDarker = HexColor('#E31F37');
  static var endangeredLighter = HexColor('#FF566A');

  static var noticably = HexColor('#FFCD1C');
  static var noticeablyDarker = HexColor('#E6BA1B');
  static var noticeablyLighter = HexColor('#FFDA58');

  // static var positive= HexColor('#05CB58');
  static var positiveDarker = HexColor('#08A84B');
  static var positiveLighter = HexColor('#37D579');

  static var prettyRed = HexColor('#FFEEEF');

  static var goldTips = HexColor('#E6BA1B');
  static var blueRibbon = HexColor('#0A65FF');
  static var athensGray = HexColor('#EBEBEC');
  static var blackOpacity02 = HexColor('#000000').withOpacity(.2);
  static var blackOpacity04 = HexColor('#000000').withOpacity(.4);
  static var pinkSalmon = HexColor('#FF8291');
  static var whiteOpacity = HexColor('#FFFFFF').withOpacity(.5);
  static var scienceBlue = HexColor('#004EC7');
  static var borderGrey = HexColor('#D7D8D9');

  static var lightGrayishBlue = HexColor('#E7EDFA');
  static var vividRed = HexColor('#ED1C24');

  static var lightGrayishCyan = HexColor('#F8F9F9');
  static var babyBlue = HexColor('#E6F9FC');

  // Misc
  static var pinkRed = HexColor('#ffeeef');
  static var fauxFloralWhite = HexColor('#FFF9F1');
  static var strawBurry = HexColor('#F84948');

  // primitive
  static var black100 = HexColor('#070D14');
  static var black90 = HexColor('#2C3137');
  static var black80 = HexColor('#454A51');
  static var black60 = HexColor('#737377');
  static var black40 = HexColor('#A0A1A2');
  static var black20 = HexColor('#D3D2D2');
  static var black10 = HexColor('#EBEAEA');
  static var black5 = HexColor('#F6F4F5');
  static var white = HexColor('#FFFFFF');

  // red
  static var red100 = HexColor('#FF353C');
  static var red75 = HexColor('#FF676D');
  static var red35 = HexColor('#FFB8BB');
  static var red15 = HexColor('#FFE1E2');
  static var redDk65 = HexColor('#591315');
  static var redDk50 = HexColor('#801A1E');
  static var redDk35 = HexColor('#A62227');
  static var redDk15 = HexColor('#D92D33');

  // yellow
  static var yellow100 = HexColor('#FFDE32');
  static var yellow75 = HexColor('#FFE665');
  static var yellow35 = HexColor('#FFF3B7');
  static var yellow15 = HexColor('#FFFAE0');
  static var yellow5 = HexColor('#FFFDF5');
  static var yellowDk65 = HexColor('#594E12');
  static var yellowDk50 = HexColor('#806F19');
  static var yellowDk35 = HexColor('#A69021');
  static var yellowDk15 = HexColor('#D9BD2B');

  // green
  static var green100 = HexColor('#34C759');
  static var green75 = HexColor('#67D582');
  static var green35 = HexColor('#B8EBC5');
  static var green15 = HexColor('#E0F7E6');
  static var green5 = HexColor('#F5FCF7');
  static var greenDk65 = HexColor('#12461F');
  static var greenDk50 = HexColor('#1A632C');
  static var greenDk35 = HexColor('#22813A');
  static var greenDk15 = HexColor('#2CA94C');

  // blue
  static var blue100 = HexColor('#0A65FF');
  static var blue75 = HexColor('#478CFF');
  static var blue35 = HexColor('#A9C9FF');
  static var blue15 = HexColor('#DAE8FF');
  static var blue5 = HexColor('#F3F7FF');
  static var blueDk65 = HexColor('#042359');
  static var blueDk50 = HexColor('#053380');
  static var blueDk35 = HexColor('#0742A6');
  static var blueDk15 = HexColor('#0851CC');

  // purple
  static var purple100 = HexColor('#6658F3');
  static var purple75 = HexColor('#8C82F6');
  static var purple35 = HexColor('#C9C5FB');
  static var purple15 = HexColor('#E8E6FD');
  static var purple5 = HexColor('#F7F7FE');
  static var purpleDk65 = HexColor('#281E43');
  static var purpleDk50 = HexColor('#332C79');
  static var purpleDk35 = HexColor('#42399E');
  static var purpleDk15 = HexColor('#574BCF');

  // teal
  static var teal100 = HexColor('#23C9BF');
  static var teal75 = HexColor('#5AD6CF');
  static var teal35 = HexColor('#B2ECE9');
  static var teal15 = HexColor('#DEF7F5');
  static var teal5 = HexColor('#F4FCFC');
  static var tealDk65 = HexColor('#0C4643');
  static var tealDk50 = HexColor('#116460');
  static var tealDk35 = HexColor('#17837C');
  static var tealDk15 = HexColor('#1EABA2');

  // background
  static var bgPrimary = HexColor('#FFFFFF');
  static var bgSecondary = HexColor('#F6F4F5');
  static var bgTertiary = HexColor('#EBEAEA');
  static var bgInversePrimary = HexColor('#000000');
  static var bgInverseSecondary = HexColor('#070D14');
  static var bgDisabled = HexColor('#F6F4F5');
  static var bgOverlayDark = HexColor('rgba(0, 0, 0, 0.3)');
  static var bgLight = HexColor('rgba(0, 0, 0, 0.08)');
  static var bgAccent = HexColor('#FF353C');
  static var bgDarkerAccent = HexColor('#D92D33');
  static var bgNegative = HexColor('#FF353C');
  static var bgWarning = HexColor('#FFDE32');
  static var bgPositive = HexColor('#34C759');
  static var bgPurple = HexColor('#F7F7FE');
  static var bgBlue = HexColor('#F3F7FF');
  static var bgBlack = HexColor('#000000');
  static var bgWhite = HexColor('#FFFFFF');

  // border
  static var borderOpaque = HexColor('#EBEAEA');
  static var borderSelected = HexColor('#FF353C');

  //content
  static var contentLarge = HexColor('#070D14');
  static var contentSecondary = HexColor('#454A51');
  static var contentTertiary = HexColor('#737377');
  static var contentInversePrimary = HexColor('#FFFFFF');
  static var contentInverseSecondary = HexColor('rgba(255, 255, 255, 0.8)');
  static var contentInverseTertiary = HexColor('rgba(255, 255, 255, 0.6)');
  static var contentOnColor = HexColor('#FFFFFF');
  static var contentAccent = HexColor('#FF353C');
  static var contentNegative = HexColor('#FF353C');
  static var contentWarning = HexColor('#D9BD2B');
  static var contentPositive = HexColor('#2CA94C');
  static var contentPurple = HexColor('#6658F3');
  static var contentBlue = HexColor('#0A65FF');
  static var contentTeal = HexColor('#1EABA2');
  static var contentGreen = HexColor('#34C759');

  // Validus
  static var contentValidusSelected = HexColor('#FFB802');
  static var contentValidusUnselected = HexColor('#A2A2AE');
  static var contentDarkTheme = HexColor('#D2D2D2');
  static var systemSuccess = HexColor('#1ACC81');
  static var systemError = HexColor('#E22716');
  static var bgCard = HexColor('#1E1E3D');
  static var bgScreen = HexColor('#171734');
  static var contentDarkthemeSubtext = HexColor('#A1A1A1');
  static var bgTextEdit = HexColor('#2F3444');
  static var bgCTA = HexColor('#FFC700');
}

// Parse hex to color.
class HexColor extends Color {
  static int _getColorFromHex(hexColor) {
    hexColor ??= '#FFFFFF';
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(value) : super(_getColorFromHex(value));
}
