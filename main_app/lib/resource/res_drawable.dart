class ResDrawable {
  static const baseAsset = 'assets/images/';
  static const icChart = baseAsset + 'ic_chart.png';
  static const icHome = baseAsset + 'ic_home.png';
  static const icArrowUpGreen = baseAsset + 'ic_arrow_up_green.png';
  static const icArrowDownRed = baseAsset + 'ic_arrow_down_red.png';
  static const icCrossX = baseAsset + 'ic_cross_x.png';
  static const icHeart = baseAsset + 'ic_heart.png';
  static const icHeartRed = baseAsset + 'ic_heart_red.png';
}
