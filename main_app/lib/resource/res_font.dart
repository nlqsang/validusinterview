class ResFonts {
  // Quicksand
  static const quicksandBold = 'QuicksandBold';
  static const quicksandBoldBig = 'QuicksandBoldBig';
  static const quicksandMedium = 'QuicksandMedium';
  static const quicksandRegular = 'QuicksandRegular';

// San Francisco Pro Display
  static const sfDisplayBold = 'SFDisplayBold';
  static const sfDisplaySemibold = 'SFDisplaySemibold';
  static const sfDisplayMedium = 'SFDisplayMedium';
  static const sfDisplayRegular = 'SFDisplayRegular';

  // San Francisco Pro Text
  static const sfTextBold = 'SFTextBold';
  static const sfTextSemibold = 'SFTextSemibold';
  static const sfTextMedium = 'SFTextMedium';
  static const sfTextRegular = 'SFTextRegular';

  // Airbnb Cereal
  static const airbnbBold = 'AirbnbCerealBold';
  static const airbnbLight = 'AirbnbCerealLight';
  static const airbnbMedium = 'AirbnbCerealMedium';
  static const airbnbRegular = 'AirbnbCerealRegular';

  // Test Founders Grotesk
  static const testFoundersGroteskBold = 'TestFoundersGroteskBold';
  static const testFoundersGroteskSemibold = 'TestFoundersGroteskSemibold';
  static const testFoundersGroteskMedium = 'TestFoundersGroteskMedium';
  static const testFoundersGroteskRegular = 'TestFoundersGroteskRegular';
  static const testFoundersGroteskLight = 'TestFoundersGroteskLight';
}
