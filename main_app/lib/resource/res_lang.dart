class ResLang {
  static const common_api_error = 'common_api_error';

  static const dialog_confirmation_title = 'dialog_confirmation_title';
  static const dialog_confirmation_ok_button = 'dialog_confirmation_ok_button';
  static const dialog_confirmation_cancel_button =
      'dialog_confirmation_cancel_button';
  static const dialog_error_title = 'dialog_error_title';
  static const dialog_close_button = 'dialog_close_button';
  static const dialog_success_title = 'dialog_success_title';
}
