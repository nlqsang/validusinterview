import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/di/injection.dart';
import 'package:main_app/flavor/flavor_config.dart';
import 'package:main_app/localization/localization_service.dart';
import 'package:main_app/main.dart';
import 'package:main_app/resource/res_style.dart';
import 'package:main_app/route/app_route.dart';

// Init lib here: locator, lang ...
void main() async => {
      WidgetsFlutterBinding.ensureInitialized(),
      await configureDependencies(),
      FlavorConfig.appFlavor = Flavor.DEV,
      runApp(
        MyApp(),
      ),
    };
