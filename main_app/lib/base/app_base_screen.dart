import 'package:base_app/core/base_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/widget/dialog/confirm_dialog_widget.dart';
import 'package:main_app/widget/dialog/error_dialog_widget.dart';
import 'package:main_app/widget/dialog/success_dialog_widget.dart';

abstract class AppBaseScreen extends BaseScreen {
  const AppBaseScreen({Key? key}) : super(key: key);

  void showErrorPopup({
    String? content,
    VoidCallback? onClose,
    bool barrierDismissible = true,
  }) {
    Get.dialog(
      ErrorDialogWidget(
        content: content,
        onClose: onClose,
      ),
      barrierDismissible: barrierDismissible,
    );
  }

  void showSuccessPopup({
    String? content,
    VoidCallback? onClose,
    bool barrierDismissible = true,
  }) {
    Get.dialog(
      SuccessDialogWidget(
        content: content,
        onClose: onClose,
      ),
      barrierDismissible: barrierDismissible,
    );
  }

  void showConfirmPopup({
    String? title,
    String? content,
    VoidCallback? onCancel,
    VoidCallback? onConfirm,
    bool barrierDismissible = true,
  }) {
    Get.dialog(
      ConfirmDialogWidget(
        title: title,
        content: content,
        onConfirm: onConfirm,
        onCancel: onCancel,
      ),
      barrierDismissible: barrierDismissible,
    );
  }

  void showToast(String message) {
    Get.showSnackbar(
      GetBar(
        message: message,
        snackPosition: SnackPosition.TOP,
        snackStyle: SnackStyle.GROUNDED,
        duration: Duration(seconds: 2),
        backgroundColor: ResColors.primaryColor,
      ),
    );
  }
}
