import 'package:json_annotation/json_annotation.dart';
import 'package:main_app/converter/local_datetime_converter.dart';

part 'stock.g.dart';

@JsonSerializable()
@LocalDateTimeConverter()
class Stock {
  Stock({
    this.id = '',
    this.stockName = '',
    this.price = 0,
    this.dayGain = 0,
    this.lastTrade = '',
    this.extendedHours = '',
  this.lastPrice = 0,
  });

  String id;
  String stockName;
  double price;
  double dayGain;
  String lastTrade;
  String extendedHours;
  double lastPrice;

  factory Stock.fromJson(Map<String, dynamic> json) =>
      _$StockFromJson(json);

  Map<String, dynamic> toJson() => _$StockToJson(this);
}
