import 'package:base_app/logger/app_logger.dart';
import 'package:flutter/services.dart';

class AppPlatformChanel {
  static const CHANNEL_NAME = 'com.data.native';
  static const appPlatform = MethodChannel(CHANNEL_NAME);
  static const FLUTTER_CHANNEL_GET_INIT_DATA_METHOD = 'getInitData';

  // TODO Demo for get data from native.
  static Future<String?> getInitData() async {
    AppLogger.d('Start call getInitData to native');
    try {
      var data =
          await appPlatform.invokeMethod(FLUTTER_CHANNEL_GET_INIT_DATA_METHOD);

      return data;
    } catch (e) {
      AppLogger.e(e);
      return null;
    }
  }
}
