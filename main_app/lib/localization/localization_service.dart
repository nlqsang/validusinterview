import 'dart:ui';

import 'package:get/get.dart';
import 'package:main_app/localization/lang/vi_vn.dart';

import 'lang/en_us.dart';

class LocalizationService extends Translations {
  // Default locale
  static final locale = Locale('vi', 'VN');

  // fallbackLocale saves the day when the locale gets in trouble
  static final fallbackLocale = Locale('vi', 'VN');

  // Supported languages
  // Needs to be same order with locales
  static final langs = [
    'English',
    'VietName',
  ];

  // Supported locales
  // Needs to be same order with langs
  static final locales = [
    Locale('en', 'US'),
    Locale('vi', 'VN'),
  ];

  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': enUS,
        'vi_VN': viVN,
      };

  // Gets locale from language, and updates the locale
  void changeLocale(String lang) {
    final locale = _getLocaleFromLanguage(lang);
    Get.updateLocale(locale ?? fallbackLocale);
  }

  // Finds language in `langs` list and returns it as Locale
  Locale? _getLocaleFromLanguage(String lang) {
    for (var i = 0; i < langs.length; i++) {
      if (lang == langs[i]) return locales[i];
    }
    return Get.locale;
  }
}
