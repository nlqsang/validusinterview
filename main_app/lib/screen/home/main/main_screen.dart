import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_drawable.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/screen/home/components/fav_stock_screen.dart';
import 'package:main_app/screen/home/components/profile_screen.dart';
import 'package:main_app/screen/home/components/stock_screen.dart';
import 'package:main_app/screen/home/main/main_controller.dart';

class MainScreen extends GetView<MainController> {
  MainScreen({Key? key}) : super(key: key);
  PageController _pageController = PageController(initialPage: 0);

  List<Widget> _bottomBars = <Widget>[
    StockScreen(),
    FavouriteStockScreen(),
    ProfileScreen()
  ];

  void _onItemTapped(int index) {
    _pageController.animateToPage(index,
        duration: Duration(milliseconds: 400), curve: Curves.ease);
  }

  void _onPageChanged(int currentPage) {
    controller.currentIndex.value = currentPage;
  }

  Widget _buildBottomNavigationBar() {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Image.asset(ResDrawable.icChart,
              width: 32, height: 32, color: ResColors.white),
          activeIcon: Image.asset(
            ResDrawable.icChart,
            width: 32,
            height: 32,
            color: ResColors.contentValidusSelected,
          ),
          label: 'Stocks',
        ),
        BottomNavigationBarItem(
          icon: Image.asset(ResDrawable.icHeart,
              width: 26, height: 26, color: ResColors.white),
          activeIcon: Image.asset(
            ResDrawable.icHeart,
            width: 26,
            height: 26,
            color: ResColors.contentValidusSelected,
          ),
          label: 'Favourite',
        ),
        BottomNavigationBarItem(
          icon: Image.asset(
            ResDrawable.icHome,
            width: 32,
            height: 32,
          ),
          activeIcon: Image.asset(
            ResDrawable.icHome,
            width: 32,
            height: 32,
            color: ResColors.contentValidusSelected,
          ),
          label: 'Profile',
        ),
      ],
      currentIndex: controller.currentIndex.value,
      backgroundColor: ResColors.bgScreen,
      selectedItemColor: ResColors.contentValidusSelected,
      selectedIconTheme: IconThemeData(color: ResColors.contentValidusSelected),
      selectedLabelStyle: _txtSelectedLabel,
      selectedFontSize: 12,
      unselectedFontSize: 12,
      unselectedLabelStyle: _txtUnselectedLabel,
      unselectedItemColor: ResColors.contentValidusUnselected,
      unselectedIconTheme:
          IconThemeData(color: ResColors.contentValidusUnselected),
      onTap: _onItemTapped,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        centerTitle: false,
        backgroundColor: ResColors.bgScreen,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: ResColors.bgScreen,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.light,
        ),
      ),
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
            color: ResColors.bgScreen,
            child: PageView(
              controller: _pageController,
              onPageChanged: _onPageChanged,
              children: _bottomBars,
            ),
          ),
          Obx(() => Visibility(
              visible: controller.isLoading.value,
              child: Center(
                child: CircularProgressIndicator(),
              )))
        ],
      ),
      bottomNavigationBar: Obx(() => _buildBottomNavigationBar()),
    );
  }
}

var _txtSelectedLabel = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: ResColors.contentValidusSelected,
    fontFamily: ResFonts.airbnbRegular);

var _txtUnselectedLabel = _txtSelectedLabel
    .merge(TextStyle(color: ResColors.contentValidusUnselected));
