import 'package:get/get.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MainController());
  }
}

class MainController extends GetxController {
  Rx<int> currentIndex = 0.obs;

  Rx<bool> isLoading = false.obs;

  MainController();

  @override
  void onInit() async {
    super.onInit();
  }
}
