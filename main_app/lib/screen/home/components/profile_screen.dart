import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/schema/profile/profile.dart';
import 'package:main_app/screen/home/components/profile_controller.dart';
import 'package:main_app/widget/profile/profile_editor_sheet.dart';
import 'package:main_app/widget/profile/profile_row_widget.dart';

class ProfileScreen extends GetView<ProfileController> {
  ProfileScreen({Key? key}) : super(key: key);

  TextEditingController _getTextEditingController(ProfileType type) {
    switch (type) {
      case ProfileType.EMAIL:
        return controller.emailController;
      case ProfileType.ADDRESS:
        return controller.addressController;
      case ProfileType.NAME:
        return controller.nameController;
    }
  }

  _onOpenProfileEditor(BuildContext context, ProfileType type) {
    var textEditingController = _getTextEditingController(type);
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      backgroundColor: ResColors.bgScreen,
      clipBehavior: Clip.antiAlias,
      barrierColor: Colors.black,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
            height: Get.height * .9,
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              child: ProfileEditorSheet(
                  type: type,
                  textEditingController: textEditingController,
                  onSave: _onSaveProfileItem),
            ));
      },
    );
  }

  _onSaveProfileItem(ProfileType type) {
    Get.back();
    var textEditingController = _getTextEditingController(type);
    controller.saveProfile(type, textEditingController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 24),
          child: Text(
            "Profile",
            style: _txtTitle,
          ),
        ),
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: [
                Obx(
                  () => ProfileItemWidget(
                      label: "Name",
                      value: controller.name.value,
                      onPress: () =>
                          _onOpenProfileEditor(context, ProfileType.NAME)),
                ),
                Obx(
                  () => ProfileItemWidget(
                      label: "Email",
                      value: controller.email.value,
                      onPress: () =>
                          _onOpenProfileEditor(context, ProfileType.EMAIL)),
                ),
                Obx(
                  () => ProfileItemWidget(
                      label: "Address",
                      value: controller.address.value,
                      onPress: () =>
                          _onOpenProfileEditor(context, ProfileType.ADDRESS)),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

var _txtTitle = TextStyle(
    fontSize: 36,
    fontWeight: FontWeight.w700,
    color: ResColors.white,
    fontFamily: ResFonts.airbnbMedium);
