import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/screen/home/components/stock_controller.dart';
import 'package:main_app/widget/stock/stock_item_widget.dart';

class StockScreen extends GetView<StockController> {
  StockScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 24),
          child: Text(
            "My watchlist",
            style: _txtTitle,
          ),
        ),
        Expanded(
            child: Obx(() => ListView.builder(
                itemCount: controller.stocks.value.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    margin: EdgeInsets.only(bottom: 16),
                    child: StockItemWidget(controller.stocks.value[index]),
                  );
                })))
      ],
    );
  }
}

var _txtTitle = TextStyle(
    fontSize: 36,
    fontWeight: FontWeight.w700,
    color: ResColors.white,
    fontFamily: ResFonts.airbnbMedium);
