import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/screen/home/components/stock_controller.dart';
import 'package:main_app/widget/stock/stock_item_widget.dart';

class FavouriteStockScreen extends GetView<StockController> {
  FavouriteStockScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 8, bottom: 24),
          child: Text(
            "Favourites",
            style: _txtTitle,
          ),
        ),
        Expanded(
            child: Obx(() => ListView.builder(
                itemCount: controller.favStocks.value.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    margin: EdgeInsets.only(bottom: 16),
                    child: StockItemWidget(controller.favStocks.value[index],),
                  );
                })))
      ],
    );
  }
}

var _txtTitle = TextStyle(
    fontSize: 36,
    fontWeight: FontWeight.w700,
    color: ResColors.white,
    fontFamily: ResFonts.airbnbMedium);
