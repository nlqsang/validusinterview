import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:main_app/data_source/local/shared_preferences_repo.dart';
import 'package:main_app/repo/stock_repo.dart';
import 'package:main_app/schema/response/api_response.dart';
import 'package:main_app/schema/stock/stock.dart';
import 'package:main_app/utils/crypto.dart';
import 'package:main_app/utils/device_info.dart';

class StockBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => StockController(
          GetIt.instance.get<IStockRepo>(),
          GetIt.instance.get<SharedPreferencesRepo>(),
        ));
  }
}

class StockController extends GetxController {
  final IStockRepo _stockRepo;
  final SharedPreferencesRepo _sharedPreferencesRepo;

  RxList<Stock> stocks = RxList<Stock>([]);
  RxList<Stock> favStocks = RxList<Stock>([]);

  StockController(this._stockRepo, this._sharedPreferencesRepo);

  @override
  void onInit() async {
    await _getStocks();
    await _getFavStocks();
    super.onInit();
  }

  Future<void> saveFavStocks() async {
    // Get Ids of favourite stocks
    var favStockIds = this.favStocks.map((element) => element.id);

    // Convert Id array to string with ',' seperator
    var favStockString = favStockIds.join(',');

    var deviceId = await getDeviceUUID();
    var encrypted = encrypt(favStockString, deviceId);

    // Store encrpyted data in local storage
    _sharedPreferencesRepo.saveFavStock(encrypted);
  }

  Future<void> _getFavStocks() async {
    var deviceId = await getDeviceUUID();
    var encrypted = _sharedPreferencesRepo.getFavStocks();

    // String of favourite stocks Id, seperated by ','
    var decrypted = decrypt(encrypted, deviceId);

    // Id array
    var favStockIds = decrypted.split(',');

    // Convert Id array to Stock array
    this.favStocks.value =
        this.stocks.where((e) => favStockIds.contains(e.id)).toList();
  }

  Future<APIResponse<List<Stock>>> _getStocks() async {
    var response = await _stockRepo.getStocks();
    if (response.statusCode == 200 && response.data != null) {
      // Success
      stocks.value = response.data ?? [];
    } else {
      // Error
    }
    return response;
  }
}
