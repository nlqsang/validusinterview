import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:main_app/data_source/local/shared_preferences_repo.dart';
import 'package:main_app/schema/profile/profile.dart';
import 'package:main_app/utils/crypto.dart';
import 'package:main_app/utils/device_info.dart';

class ProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProfileController(
          GetIt.instance.get<SharedPreferencesRepo>(),
        ));
  }
}

class ProfileController extends GetxController {
  final SharedPreferencesRepo _sharedPreferencesRepo;

  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  Rx<String> email = "".obs;
  Rx<String> name = "".obs;
  Rx<String> address = "".obs;

  ProfileController(this._sharedPreferencesRepo);

  @override
  void onInit() async {

    // Get profile from local storage
    this.email.value = await getProfile(ProfileType.EMAIL);
    this.emailController.text = email.value;

    this.name.value = await getProfile(ProfileType.NAME);
    this.nameController.text = name.value;

    this.address.value = await getProfile(ProfileType.ADDRESS);
    this.addressController.text = address.value;

    super.onInit();
  }

  Future<String> getProfile(ProfileType type) async {
    // Decrypt the cipher from local storage
    var deviceId = await getDeviceUUID();
    var encrypted = _sharedPreferencesRepo.getProfile(type);
    print('ENCRYPTED: ${encrypted}');
    return decrypt(encrypted, deviceId);
  }

  Future<void> saveProfile(ProfileType type, String value) async {
    // Encrypt the data
    var deviceId = await getDeviceUUID();
    var encrypted = encrypt(value, deviceId);
    print('ENCRYPTED: ${encrypted}');

    // Store encrpyted data in local storage
    await _sharedPreferencesRepo.saveProfile(type, encrypted);
    switch (type) {
      case ProfileType.EMAIL:
        email.value = value;
        break;
      case ProfileType.ADDRESS:
        address.value = value;
        break;
      case ProfileType.NAME:
        name.value = value;
        break;
    }
  }
}
