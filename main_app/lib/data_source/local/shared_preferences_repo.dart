import 'package:base_app/data_source/local/shared_preferences_repo.dart';
import 'package:injectable/injectable.dart';
import 'package:main_app/schema/profile/profile.dart';

@singleton
class SharedPreferencesRepo extends BaseSharedPreferencesRepo {
  final TOKEN_KEY = 'TOKEN_KEY';
  final NAME_KEY = 'NAME_KEY';
  final ADDRESS_KEY = 'ADDRESS_KEY';
  final EMAIL_KEY = 'EMAIL_KEY';
  final FAV_STOCK_KEY = 'FAV_STOCK_KEY';

  Future<void> saveToken(String? token) {
    return saveData(TOKEN_KEY, token);
  }

  String? getToken() {
    return readData(TOKEN_KEY);
  }

  String _getProfileKey(ProfileType type) {
    switch (type) {
      case ProfileType.EMAIL:
        return EMAIL_KEY;
      case ProfileType.ADDRESS:
        return ADDRESS_KEY;
      case ProfileType.NAME:
        return NAME_KEY;
    }
  }

  Future<void> saveProfile(ProfileType type, String value) {
    var key = _getProfileKey(type);
    return saveData(key, value);
  }

  String getProfile(ProfileType type) {
    var key = _getProfileKey(type);
    return readData(key);
  }

  Future<void> saveFavStock(String stocks) {
    return saveData(FAV_STOCK_KEY, stocks);
  }

  String getFavStocks() {
    return readData(FAV_STOCK_KEY);
  }

  Future<void> clearData() async {
    await Future.wait([
      box.remove(TOKEN_KEY),
    ]);
  }
}
