import 'package:dio/dio.dart';
import 'package:main_app/schema/response/api_response.dart';
import 'package:main_app/schema/stock/stock.dart';
import 'package:retrofit/http.dart';

part 'stock_service.g.dart';

@RestApi()
abstract class StockService {

  factory StockService(Dio dio, {String baseUrl}) = _StockService;

  @GET('/fc3ddccf-855c-4bb6-861c-cf7896aa963e')
  Future<APIResponse<List<Stock>>> getStocks();
}
