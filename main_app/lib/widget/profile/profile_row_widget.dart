import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_font.dart';

class ProfileItemWidget extends StatelessWidget {
  String label;
  String value;
  final void Function() onPress;

  ProfileItemWidget(
      {required this.label, required this.value, required this.onPress});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 27, bottom: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                label,
                style: _txtItemLabel,
              ),
              GestureDetector(
                onTap: onPress,
                child: Text(
                  "Edit",
                  style: _txtEdit,
                ),
              )
            ],
          ),
          SizedBox(
            height: 16,
          ),
          if (value.isEmpty) ...[
            Text(
              label +
                  ' is currently empty. Please fill the information',
              style: _txtError,
            )
          ],
          Text(
            value,
            style: _txtItemValue,
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }
}

var _txtItemLabel = TextStyle(
    fontSize: 16,
    fontFamily: ResFonts.testFoundersGroteskLight,
    color: ResColors.white,
    fontWeight: FontWeight.w400);

var _txtEdit = TextStyle(
    fontSize: 18,
    fontFamily: ResFonts.testFoundersGroteskRegular,
    color: ResColors.white,
    decoration: TextDecoration.underline,
    fontWeight: FontWeight.w400);

var _txtItemValue = TextStyle(
    fontSize: 20,
    fontFamily: ResFonts.testFoundersGroteskRegular,
    color: ResColors.contentDarkthemeSubtext,
    fontWeight: FontWeight.w400);

var _txtError = TextStyle(
    fontSize: 12,
    fontFamily: ResFonts.testFoundersGroteskLight,
    color: ResColors.bgCTA,
    fontWeight: FontWeight.w400);
