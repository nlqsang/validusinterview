import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_drawable.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/schema/profile/profile.dart';
import 'package:main_app/screen/home/components/profile_controller.dart';
import 'package:main_app/screen/home/main/main_controller.dart';
import 'package:main_app/widget/common/text_button.dart';
import 'package:main_app/widget/common/text_input.dart';

class ProfileEditorSheet extends GetView<ProfileController> {
  ProfileType type;
  TextEditingController textEditingController;
  final void Function(ProfileType type) onSave;

  GlobalObjectKey<FormState> _formKey = const GlobalObjectKey<FormState>('form');

  ProfileEditorSheet(
      {required this.type,
      required this.textEditingController,
      required this.onSave});

  _onSaveProfile() {
    var isValid = _formKey.currentState!.validate();
    if (isValid) {
      this.onSave(this.type);
    }
  }

  _onClose() {
    Get.back();
  }

  _genTitle() {
    switch (type) {
      case ProfileType.EMAIL:
        return 'email';
      case ProfileType.NAME:
        return 'name';
      case ProfileType.ADDRESS:
        return 'address';
    }
  }

  _genLabel() {
    switch (type) {
      case ProfileType.EMAIL:
        return 'Email address';
      case ProfileType.NAME:
        return 'Name';
      case ProfileType.ADDRESS:
        return 'Address';
    }
  }

  MultiValidator _genValidator() {
    switch (type) {
      case ProfileType.EMAIL:
        return MultiValidator([
          RequiredValidator(errorText: "Required"),
          EmailValidator(errorText: "Incorrect email format"),
        ]);
      case ProfileType.NAME:
        return MultiValidator([
          RequiredValidator(errorText: "Required"),
          MinLengthValidator(4,
              errorText: "Name must have at least 4 characters"),
          MaxLengthValidator(256,
              errorText: "Name cannot be longer than 256 characters")
        ]);
      case ProfileType.ADDRESS:
        return MultiValidator([
          RequiredValidator(errorText: "Required"),
          MinLengthValidator(4,
              errorText: "Address must have at least 4 characters"),
          MaxLengthValidator(256,
              errorText: "Address cannot be longer than 256 characters")
        ]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ResColors.bgScreen,
      padding: EdgeInsets.all(24),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                child: Image.asset(
                  ResDrawable.icCrossX,
                  width: 16,
                  height: 16,
                ),
                onTap: _onClose,
              ),
              Text(
                "Edit " + _genTitle(),
                style: _txtTitle,
              ),
              Container(
                width: 16,
              )
            ],
          ),
          SizedBox(
            height: 40,
          ),
          if (this.type == ProfileType.EMAIL) ...[
            Text(
              "We'll send you an email to confirm you new email address",
              style: _txtEmailDescription,
            ),
            SizedBox(
              height: 24,
            ),
          ],
          Form(
            key: _formKey,
            child: AppTextInput(
              "Please enter your " + _genTitle(),
              textEditingController,
              fillColor: ResColors.bgTextEdit,
              borderRadius: 4,
              label: _genLabel(),
              validator: _genValidator(),
              contentPadding: EdgeInsets.all(0),
              inputTextStyle: _txtEditor,
            ),
          ),
          Spacer(),
          AppTextButton(
            "Save",
            _onSaveProfile,
            textStyle: _txtBtn,
            padding: EdgeInsets.all(12),
            backgroundColor: ResColors.bgCTA,
          )
        ],
      ),
    );
  }
}

var _txtTitle = TextStyle(
    fontSize: 18,
    fontFamily: ResFonts.airbnbRegular,
    color: ResColors.white,
    fontWeight: FontWeight.w400);

var _txtEmailDescription = TextStyle(
    fontSize: 16,
    fontFamily: ResFonts.testFoundersGroteskRegular,
    color: ResColors.white,
    fontWeight: FontWeight.w400);

var _txtBtn = TextStyle(
    fontSize: 20,
    height: 1.5,
    fontFamily: ResFonts.testFoundersGroteskLight,
    color: Colors.black,
    fontWeight: FontWeight.w400);

var _txtEditor = TextStyle(
    fontSize: 16,
    height: 1.5,
    fontFamily: ResFonts.testFoundersGroteskRegular,
    color: ResColors.contentDarkTheme,
    fontWeight: FontWeight.w400);
