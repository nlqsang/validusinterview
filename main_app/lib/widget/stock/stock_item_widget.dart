import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_drawable.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/schema/stock/stock.dart';
import 'package:main_app/screen/home/components/stock_controller.dart';
import 'package:main_app/utils/datetime.dart';
import 'package:main_app/widget/stock/stock_row_widget.dart';

class StockItemWidget extends GetView<StockController> {
  Stock stock;
  StockItemWidget(this.stock);

  _onFavourite() async {
    if (!controller.favStocks.value.contains(this.stock))
      controller.favStocks.add(this.stock);
    else
      controller.favStocks.remove(this.stock);
    await controller.saveFavStocks();
  }

  @override
  Widget build(BuildContext context) {
    var differentPercentage =
        (stock.lastPrice - stock.price).abs() * 100 / stock.lastPrice;
    var lastTrade = dateStringFromTimestamp(int.tryParse(stock.lastTrade) ?? 0);
    var isIncrease = stock.lastPrice - stock.price < 0;
    var extendedHours =
        dateStringFromTimestamp(int.tryParse(stock.extendedHours) ?? 0);

    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          color: ResColors.bgCard, borderRadius: BorderRadius.circular(4)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                stock.stockName,
                style: _txtTitle,
              ),
              Obx(() => GestureDetector(
                  onTap: _onFavourite,
                  child: Image.asset(
                    controller.favStocks.value.contains(stock)
                        ? ResDrawable.icHeartRed
                        : ResDrawable.icHeart,
                    width: 24,
                    height: 24,
                    color: controller.favStocks.value.contains(stock)
                        ? ResColors.red75
                        : ResColors.white,
                  )))
            ],
          ),
          SizedBox(
            height: 8,
          ),
          StockRowWidget(label: "Price", value: '${stock.price}'),
          StockRowWidget(label: "Day gain", value: '${stock.dayGain}'),
          StockRowWidget(label: "Last trade", value: lastTrade),
          StockRowWidget(label: "Extended hrs", value: extendedHours),
          StockRowPercentageWidget(
              label: "% Change",
              value:
                  ' ${isIncrease ? '+' : '-'}${differentPercentage.toStringAsFixed(1)}',
              isIncrease: isIncrease),
        ],
      ),
    );
  }
}

var _txtTitle = TextStyle(
    fontSize: 20,
    fontFamily: ResFonts.testFoundersGroteskMedium,
    color: ResColors.white,
    fontWeight: FontWeight.w400);
