import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_drawable.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/schema/stock/stock.dart';

class StockRowWidget extends StatelessWidget {
  String label;
  String value;
  StockRowWidget({required this.label, required this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: _txtItemLabel,
          ),
          Text(
            value,
            style: _txtItemValue,
          )
        ],
      ),
    );
  }
}

class StockRowPercentageWidget extends StatelessWidget {
  String label;
  String value;
  bool isIncrease;
  StockRowPercentageWidget(
      {required this.label, required this.value, required this.isIncrease});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            label,
            style: _txtItemLabel,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // If stock is increasing -> show green up-arrow. Otherwise show red down-arrow
              Image.asset(isIncrease
                  ? ResDrawable.icArrowUpGreen
                  : ResDrawable.icArrowDownRed, width: 8, height: 8,),

              SizedBox(
                width: 8,
              ),

              // If stock is increasing -> show green text. Otherwise show red text
              Text(
                '${value}%',
                style:
                    _txtItemValue.merge(isIncrease ? _colorGreen : _colorRed),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

var _txtItemLabel = TextStyle(
    fontSize: 18,
    fontFamily: ResFonts.testFoundersGroteskRegular,
    color: ResColors.contentDarkTheme,
    height: 1.2,
    fontWeight: FontWeight.w400);

var _txtItemValue = TextStyle(
    fontSize: 18,
    fontFamily: ResFonts.testFoundersGroteskRegular,
    height: 1.2,
    color: ResColors.white,
    fontWeight: FontWeight.w400);

var _colorGreen = TextStyle(color: ResColors.systemSuccess);

var _colorRed = TextStyle(color: ResColors.systemError);
