import 'package:flutter/material.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_font.dart';

class AppTextInput extends StatelessWidget {
  String placeholder;
  TextEditingController controller;
  Color fillColor;
  double borderRadius;
  EdgeInsetsGeometry margin;
  bool isObscure;
  String? Function(String?)? validator;
  final Key? key;
  int maxLength;
  TextInputType keyboardType;
  String? errorMessage;
  Function(String?)? onChange;
  String? label;
  bool? isAutoValidate;
  String? initialValue;
  TextStyle? inputTextStyle;
  EdgeInsets? contentPadding;
  bool? readOnly;
  bool? enabled;
  bool? autoFocus;

  AppTextInput(
    this.placeholder,
    this.controller, {
    this.fillColor = const Color(0xFFFFFFFF),
    this.borderRadius = 0,
    this.validator,
    this.key,
    this.maxLength = 40,
    this.isObscure = false,
    this.keyboardType = TextInputType.name,
    this.errorMessage,
    this.onChange,
    this.label,
    this.contentPadding = const EdgeInsets.symmetric(horizontal: 12),
    this.inputTextStyle,
    this.isAutoValidate = false,
    this.margin = const EdgeInsets.all(0),
    this.readOnly,
    this.enabled,
    this.autoFocus,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
            padding: margin,
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                decoration: BoxDecoration(
                    color: fillColor,
                    borderRadius: BorderRadius.circular(borderRadius)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (this.label != null) ...[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          label!,
                          style: _txtLabel,
                        ),
                      ),
                      SizedBox(height: 4)
                    ],
                    TextFormField(
                      key: key,
                      controller: controller,
                      obscureText: isObscure,
                      maxLength: maxLength,
                      style: inputTextStyle,
                      enabled: enabled ?? true,
                      readOnly: readOnly ?? false,
                      autofocus: autoFocus ?? false,
                      decoration: InputDecoration(
                          counterText: "",
                          isDense: true,
                          contentPadding: contentPadding,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(borderRadius),
                            borderSide: BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                          filled: true,
                          fillColor: fillColor,
                          errorText: errorMessage,
                          errorMaxLines: 2,
                          hintText: placeholder),
                      keyboardType: keyboardType,
                      enableSuggestions: false,
                      onChanged: onChange,
                      autovalidateMode: isAutoValidate!
                          ? AutovalidateMode.onUserInteraction
                          : AutovalidateMode.disabled,
                      validator: validator,
                    ),
                  ],
                )))
      ],
    );
  }
}

var _txtLabel = TextStyle(
  fontSize: 14,
  fontFamily: ResFonts.testFoundersGroteskLight,
  fontWeight: FontWeight.w300,
  color: ResColors.contentDarkthemeSubtext,
);
