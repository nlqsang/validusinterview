import 'package:base_app/widget/dialog/base_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_dimen.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/resource/res_lang.dart';
import 'package:get/get.dart';

class SuccessDialogWidget extends BaseDialogWidget {
  final String? title;
  final String? content;
  final VoidCallback? onClose;

  SuccessDialogWidget({
    this.title,
    @required this.content,
    this.onClose,
  }) : assert(content != null);

  @override
  Widget buildDialogContent(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(ResDimens.appMargin),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 5,
            offset: const Offset(0.0, 5),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        // To make the card compact
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.check_circle,
                color: ResColors.successColor,
              ),
              SizedBox(width: 8),
              Text(
                title ?? ResLang.dialog_success_title.tr,
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: ResFonts.quicksandBoldBig,
                  color: ResColors.successColor,
                ),
              ),
            ],
          ),
          SizedBox(height: ResDimens.appMargin),
          Text(
            content!,
            style: TextStyle(
              fontSize: 16,
              fontFamily: ResFonts.quicksandBold,
            ),
          ),
          SizedBox(height: ResDimens.appMargin),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                width: 100,
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (onClose != null) {
                      onClose!();
                    }
                  },
                  child: Text(
                    ResLang.dialog_close_button.tr,
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: ResFonts.quicksandBold,
                      color: ResColors.successColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
