import 'package:base_app/widget/dialog/base_dialog_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:main_app/resource/res_color.dart';
import 'package:main_app/resource/res_dimen.dart';
import 'package:main_app/resource/res_font.dart';
import 'package:main_app/resource/res_lang.dart';

class ConfirmDialogWidget extends BaseDialogWidget {
  final String? title;
  final String? content;
  final VoidCallback? onConfirm;
  final VoidCallback? onCancel;

  ConfirmDialogWidget({
    this.title,
    @required this.content,
    this.onConfirm,
    this.onCancel,
  }) : assert(content != null);

  @override
  Widget buildDialogContent(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(ResDimens.appMargin),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 5,
            offset: const Offset(0.0, 5),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        // To make the card compact
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.help,
                color: ResColors.primaryColor,
              ),
              SizedBox(width: 8),
              Text(
                title ?? ResLang.dialog_confirmation_title.tr,
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: ResFonts.quicksandBoldBig,
                  color: ResColors.primaryColor,
                ),
              ),
            ],
          ),
          SizedBox(height: ResDimens.appMargin),
          Row(
            children: [
              Expanded(
                child: Text(
                  content ?? '',
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: ResFonts.quicksandBold,
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: ResDimens.appMargin),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                width: 80,
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (onCancel != null) {
                      onCancel!();
                    }
                  },
                  child: Text(
                    ResLang.dialog_confirmation_cancel_button.tr,
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: ResFonts.quicksandBold,
                      color: ResColors.inputGrayColor,
                    ),
                  ),
                ),
              ),
              Container(
                width: 120,
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (onConfirm != null) {
                      onConfirm!();
                    }
                  },
                  child: Text(
                    ResLang.dialog_confirmation_ok_button.tr,
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: ResFonts.quicksandBold,
                      color: ResColors.primaryColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
