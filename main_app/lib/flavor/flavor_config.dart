enum Flavor { DEV, STG, PROD }

class FlavorConfig {
  static Flavor appFlavor = Flavor.DEV;

  static String get apiEndPoint {
    switch (appFlavor) {
      case Flavor.DEV:
        return 'https://run.mocky.io/v3';

      case Flavor.STG:
        return 'https://run.mocky.io/v3';

      case Flavor.PROD:
        return 'https://run.mocky.io/v3';
    }
  }
}
