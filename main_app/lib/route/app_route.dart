import 'package:get/get.dart';
import 'package:main_app/screen/home/components/profile_controller.dart';
import 'package:main_app/screen/home/components/stock_controller.dart';
import 'package:main_app/screen/home/main/main_controller.dart';
import 'package:main_app/screen/home/main/main_screen.dart';

const ROUTE_MAIN_SCREEN = '/ROUTE_MAIN_SCREEN';

var pages = [
  GetPage(
    name: ROUTE_MAIN_SCREEN,
    page: () => MainScreen(),
    bindings: [MainBinding(), StockBinding(), ProfileBinding()],
  ),
];
