import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';

Future<String> getDeviceUUID() async {
  var defaultKey = "validus";
  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    var iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.identifierForVendor ?? defaultKey;
  } else {
    var androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.androidId ?? defaultKey;
  }
}
