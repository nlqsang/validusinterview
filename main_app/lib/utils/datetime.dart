import 'package:intl/intl.dart';

String dateStringFromTimestamp(int timestamp) {
  var dt = DateTime.fromMillisecondsSinceEpoch(timestamp);

// 12 Hour format:
  return DateFormat('h:mm a').format(dt); //10:00 PM
}
