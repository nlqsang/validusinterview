import 'package:encrypt/encrypt.dart';

String decrypt(String cipher, String key) {
  final utf8Key = Key.fromUtf8(key);
  final iv = IV.fromLength(16);

  final encrypter = Encrypter(AES(utf8Key, padding: null));
  final decrypted = encrypter.decrypt(Encrypted.from64(cipher), iv: iv);

  return decrypted;
}

String encrypt(String text, String key) {
  final utf8Key = Key.fromUtf8(key);
  final iv = IV.fromLength(16);

  final encrypter = Encrypter(AES(utf8Key, padding: null));

  final encrypted = encrypter.encrypt(text, iv: iv);
  return encrypted.base64;
}
